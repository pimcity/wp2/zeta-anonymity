# zanon

A module to anonymize data streams with zero-delay called z-anonymity.

When instantiating the *zanon* object, the constructor receives a value in seconds for **Delta_t**, the desired probability of reaching k-anonymity expressed with a value between 0 and 1 (**pk**) and a value for **k**.

The *anonymize()* method accepts a tuple with 3 arguments **(t,u,a)** , meaning that at time **t** a user **u** exposes an attribute **a**.
Also a tuple of kind **(t,u, latitude, longitude)** is handled (in that case you need the `pyproj` module). 

If the tuple exposes an attribute that has not been exposed by at least other **z - 1** users in the past **Delta_t**, the tuple is simply ignored. Otherwise, the tuple is printed in the file 'output.txt'.

The algorithm can handle generalization when providing the attribute with a hierarchy using \* as separator (*max_generalization\*...\*min_generalization\*attribute*).
Whenever releasing the attribute is not possible, the algorithm will look for the most specific generalization exposed by at least other **z - 1** users in the past **Delta_t**. If none is found, nothing is print out.
For the geolocation case, the algorithm divides in cells the territory (only Italy) with cells of differents sizes and outputs the cell with at most 5 level of details (3km, 5km, 10km, 30km, 500km).


### Other methods

Run the *study_output.py* after the simulation to plot the distribution of z, pk and traffic during time. (You will find the plot 'z_tuning.pdf').

*endFiles()*: the file output.txt and counters.txt are intended for testing. They need to be closed at the end of the simulation (see example below).

*duration()* print the range of time covered.

## Install
```
pip install zanon
```
[Link to PyPI](https://pypi.org/project/zanon/)


## Example of usage
```python
from zanonymity import zanon

file_in = "trace.txt"
deltat = 3600 #in seconds
pk = 0.8
k = 2

z = zanon.zanon(deltat, pk, k)

for line in open(file_in, 'r'):
    t,u,a = line.split(",")
    z.anonymize((t,u,a))
z.endFiles()
z.duration()

```
